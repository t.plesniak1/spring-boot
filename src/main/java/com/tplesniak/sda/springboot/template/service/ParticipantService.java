package com.tplesniak.sda.springboot.template.service;

import com.tplesniak.sda.springboot.template.domain.Participant;
import com.tplesniak.sda.springboot.template.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParticipantService {

    @Autowired
    private ParticipantRepository participantRepository;

    public List<Participant> getParticipants() {
        return participantRepository.findAll();
    }

    public Participant saveParticipant(Participant participant){
     return participantRepository.save(participant);
    }

    public Optional<Participant> getParticipant(Long id) {
        return participantRepository.findById(id);
    }

}
