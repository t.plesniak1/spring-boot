package com.tplesniak.sda.springboot.template.domain;

public enum Accessibility {
    PRIVATE, PUBLIC
}
