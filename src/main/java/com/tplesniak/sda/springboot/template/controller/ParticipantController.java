package com.tplesniak.sda.springboot.template.controller;

import com.tplesniak.sda.springboot.template.domain.Participant;
import com.tplesniak.sda.springboot.template.service.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/participants")
public class ParticipantController {

    @Autowired
    private ParticipantService participantService;

    @GetMapping
    public ResponseEntity<List<Participant>> findAll() {
        return ResponseEntity.ok(participantService.getParticipants());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Participant> findOne(@PathVariable Long id) {

        Optional<Participant> participant = participantService.getParticipant(id);

        if (participant.isPresent()) {
            return ResponseEntity.ok(participant.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Participant participant) {
        //return ResponseEntity.ok(eventService.saveEvent(event));

        Participant savedParticipant = participantService.saveParticipant(participant);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedParticipant.getId()).toUri();

        return ResponseEntity.created(location).build();


    }


}
