package com.tplesniak.sda.springboot.template.repository;

import com.tplesniak.sda.springboot.template.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface EventRepository extends JpaRepository<Event, Long> {

    List<Event> getByDateBetween(LocalDateTime startDate, LocalDateTime endDate);

}
