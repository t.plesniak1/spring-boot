package com.tplesniak.sda.springboot.template.repository;

import com.tplesniak.sda.springboot.template.domain.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParticipantRepository extends JpaRepository<Participant, Long> {
}
