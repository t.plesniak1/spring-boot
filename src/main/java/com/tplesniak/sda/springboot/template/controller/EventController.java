package com.tplesniak.sda.springboot.template.controller;

import com.tplesniak.sda.springboot.template.domain.Event;
import com.tplesniak.sda.springboot.template.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Controller    // This means that this class is a Controller
@RequestMapping(path = "/events") // This means URL's start with /demo (after Application path)
public class EventController {

    @Autowired
    private EventService eventService;

    @GetMapping
    public ResponseEntity<List<Event>> findAll() {
        return ResponseEntity.ok(eventService.getEvents());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Event> findOne(@PathVariable Long id) {

        Optional<Event> event = eventService.getEvent(id);

        if (event.isPresent()) {
            return ResponseEntity.ok(event.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Event event) {
        //return ResponseEntity.ok(eventService.saveEvent(event));

         /**
         * H A T E O A S - styl pisania API typu REST
         *  Hypermedia As The Engine Of Application State
         */
        Event created = eventService.saveEvent(event);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(created.getId()).toUri();

        return ResponseEntity.created(location).build();

    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delete (@PathVariable Long id) {
        eventService.deleteEvent(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity updateEvent(@PathVariable Long id, @Valid @RequestBody Event event) {

        eventService.update(id, event);

        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/searchByDate")
    public ResponseEntity<List<Event>> getEventsBetweenTwoDates(@RequestParam String startDate, @RequestParam String endDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime start = LocalDateTime.parse(startDate, formatter);
        LocalDateTime end = LocalDateTime.parse(endDate, formatter);

        return ResponseEntity.ok(eventService.getEventsBetweenDates(start, end));
    }


}
